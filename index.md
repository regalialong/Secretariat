# Secretariat - Personal CRM

> *The office or department of a government secretary.*

- https://en.wiktionary.org/wiki/secretariat

Status: Very beta, basic functionality works, navigation is awkward. Javascript is code smelly, several things require toggles




Secretariat is a hand made personal CRM, a rolodex for the people you care about.
I wish to heavily borrow elements of Monica and refine them more specifically for digital personal relationships.
I don't have bad blood with the developers, but they're right now in an awkward transition out of beta with their new
chandler branch; this project exists to serve my niche.

![screenshot.png](screenshot.png)
# Terminology

## Audits

Audits as of writing have three types:

- System: Organizational / system notice messages, such as justifications for decommissions
- Event: Something specific that happened, recollected for long term recallation
- Ramble: Minor notes

## Delete, Decommission and Takedowns

Because of my online relations, I require a more fluid state system for contacts. During my time with Monica, I
leveraged the
archive function with
the concept of decommission as a soft-delete. Decommissions disallow interaction with the profile.

Decommissions are voluntary, after deciding that the profile isn't needed anymore (falling out of contact,
collecting details aren't needed, etc.)

#### Takedowns

Some people, however, don't like the idea of being benevolently profiled which is why Takedowns also are a
state.

While functionally equivalent with Decoms, their state is more sensitive because they don't want to be
profiled. No further work should be done on the
profile without **_explicit consent_** to continue from them.
Takedowns right now are done manually, we might allow other people to view their profile and request a takedown
themselves.

Gorm technically doesn't delete fully but simply sets `deleted_at`, which excludes it from queries.
Proper deletions can be done per `db.Unscoped().Delete(&obj)` though.

# Morality

This discussion is important, and I want to play the best argument I can for this.
Intent is important for a tool like this. While I mention the word "profiling" in this Readme, it's not actually meant
for profiling as much as it's there to help my memory. Important events and things eventually disappear from my memory,
that is what Secretariat is for.

For my usage, I allow anybody to opt out (see Takedowns) so people can make the decision themselves.

The problem with discussing this tool is drawing a line at what point writing stuff I want to remember about a person
becomes too much. I don't think Birthdays are anything people care about for example.

I think the development of this is justified with good intent from me, I don't want to stalk people as much as remember
them. That can be difficult with my autistic brain and how fast the web moves. 