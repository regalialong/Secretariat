package main

import (
	"gorm.io/gorm"
	"time"
)

// Person represents a real person in the database.
// Some parts of these must be nullable since they are otherwise not updatable by Gorm.
// (Gorm does not update false values, such as 0 or false)
type Person struct {
	gorm.Model

	Name            string  `json:"name"`
	Status          *Status `json:"status"`
	Nickname        string  `json:"nickname"`
	PrefersNickname *bool   `json:"prefersNickname"`
	Avatar          string  `json:"avatar"` // TODO: transition this to file upload system
	Summary         string  `json:"summary"`

	Pronouns string `json:"pronouns"`
	Gender   string `json:"gender"`

	DateOfBirth time.Time `json:"dateOfBirth"`
	Employment  string    `json:"employment"`

	OnlineAccounts []OnlineAccount `json:"accounts" gorm:"foreignKey:PersonID"`
	AuditLog       []AuditLog      `json:"audit" gorm:"foreignKey:PersonID"`
	Relationship   []Relationship  `json:"relationships" gorm:"foreignKey:PersonID"`
}

// OnlineAccount represents the digital accounts a Person possesses.
type OnlineAccount struct {
	gorm.Model `json:"-"`

	PlatformName string `json:"platformName"` // Name of the platform, friendly
	UID          string `json:"uid"`          // Unique *platform-specific* identifier
	UName        string `json:"uname"`        // Platform-specific friendly name that may not be unique
	Uri          string `json:"uri"`
	PersonID     string `json:"-"`
}

// AuditLog are logs that talk about a specific event or something that should be noted.
type AuditLog struct {
	gorm.Model
	Type    AuditType `json:"type"`
	Content string    `json:"content"`
	Date    time.Time `json:"date"`

	PersonID uint `json:"personid"`
}

// Relationship specifies a bidirectional relationship between two Person(s).
// They aren't inherently bidirectional and must be resolved manually to be relevant to a Person.
type Relationship struct {
	gorm.Model       `json:"-"`
	PersonID         uint `json:"-"`
	ToPersonID       uint `json:"ToPersonID"`
	RelationshipType `json:"RelationshipType"`
}

type RelationshipType int64

const (
	friend = iota
	spouse
	partner
	ex
	colleague
)

type Status int64

const (
	active         = iota
	decommissioned // Profile isn't needed anymore
	requested      // Person outlined asked for takedown
)

type AuditType int

const (
	system = iota // Decommission info, etc
	event         // Significant thing that happened
	ramble
)
