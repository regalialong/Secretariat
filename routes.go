package main

import (
	"errors"
	"fmt"
	"github.com/gofiber/fiber/v2"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
	"strconv"
	"time"
)

func stringtoid(str string) (uint, error) {
	var uintValue uint64
	uintValue, err = strconv.ParseUint(str, 10, 0)

	if err != nil {
		fmt.Println("Error:", err)
		return 0, err
	}

	uintResult := uint(uintValue)
	return uintResult, nil
}

func createPerson(c *fiber.Ctx) error {
	var newPerson Person
	err = c.BodyParser(&newPerson)
	if err != nil {
		return c.Status(500).SendString(err.Error())
	}
	db.Preload(clause.Associations).Create(&newPerson)

	return c.JSON(newPerson)
}

func queryPerson(c *fiber.Ctx) error {
	id := c.Params("id")
	if id == "" {
		return fiber.ErrBadRequest
	}

	var query Person
	result := db.Preload(clause.Associations).First(&query, "id = ?", id)
	if result.Error != nil {
		if errors.Is(result.Error, gorm.ErrRecordNotFound) {
			return c.Status(404).SendString("Person not found")
		}
		return c.Status(500).SendString("Error querying the database")
	}

	return c.JSON(query)
}

func updatePerson(c *fiber.Ctx) error {
	id := c.Params("id")
	if id == "" {
		return fiber.ErrBadRequest
	}

	var query Person
	result := db.Preload(clause.Associations).First(&query, "id = ?", id)
	err = c.BodyParser(&query)
	result.Updates(&query)

	if errors.Is(result.Error, gorm.ErrRecordNotFound) {
		return c.Status(404).SendString("Person not found")
	} else if result.Error != nil {
		fmt.Println(result.Error)
		return c.Status(500).SendString("Update failed, call an admin: " + result.Error.Error())
	}

	return c.JSON(query)
}

func getPersonName(c *fiber.Ctx) error {
	id := c.Params("id")
	if id == "" {
		return fiber.ErrBadRequest
	}
	var query Person
	result := db.Preload(clause.Associations).First(&query, "id = ?", id)
	if errors.Is(result.Error, gorm.ErrRecordNotFound) {
		return c.Status(404).SendString("Person not found")
	} else if result.Error != nil {
		fmt.Println(result.Error)
		return c.Status(500).SendString("Requested failed, call an admin: " + result.Error.Error())
	}

	if *query.PrefersNickname == true {
		return c.SendString(query.Nickname)
	} else {
		return c.SendString(query.Name)
	}
}

func deletePerson(c *fiber.Ctx) error {
	id := c.Params("id")
	if id == "" {
		return fiber.ErrBadRequest
	}
	var query Person
	result := db.First(&query, "id = ?", id)
	if errors.Is(result.Error, gorm.ErrRecordNotFound) {
		return c.Status(404).SendString("Person not found")
	}

	db.Delete(&query)

	if db.Error != nil {
		return c.Status(500).SendString(db.Error.Error())
	}

	return c.SendString("Done")
}

func managePersonDecomStatus(c *fiber.Ctx) error {
	id := c.Params("id")
	if id == "" {
		return fiber.ErrBadRequest
	}

	var query Person
	result := db.Preload(clause.Associations).First(&query, "id = ?", id)

	if c.Path() == "/person/"+id+"/decom" {
		*query.Status = decommissioned
	} else if c.Path() == "/person/"+id+"/recom" {
		*query.Status = active
	} else {
		// add logging here
		return c.Status(500).SendString("route detection shat itself, call admin")
	}

	result.Updates(&query)

	if errors.Is(result.Error, gorm.ErrRecordNotFound) {
		return c.Status(404).SendString("Person not found")
	} else if result.Error != nil {
		fmt.Println(result.Error)
		return c.Status(500).SendString("Update failed, call an admin: " + result.Error.Error())
	}

	return c.JSON(query)
}

func queryAudit(c *fiber.Ctx) error {
	id := c.Params("id")
	if id == "" {
		return fiber.ErrBadRequest
	}
	var query Person
	db.Preload("AuditLog").First(&query, "id = ?", id)
	if db.Error != nil {
		if errors.Is(db.Error, gorm.ErrRecordNotFound) {
			return c.Status(404).SendString("Audit not found")
		}
		return c.Status(500).SendString(db.Error.Error())
	}

	return c.JSON(query.AuditLog)
}
func addAudit(c *fiber.Ctx) error {
	id := c.Params("id")
	if id == "" {
		return fiber.ErrBadRequest
	}
	var query Person
	db.Preload("AuditLog").First(&query, "id = ?", id)
	if db.Error != nil {
		return c.Status(500).SendString(db.Error.Error())
	}

	var request AuditLog
	err = c.BodyParser(&request)
	if err != nil {
		return c.Status(500).SendString(err.Error())
	}

	request.Date = time.Now()
	query.AuditLog = append(query.AuditLog, request)
	tx := db.Preload("AuditLog").Updates(&query)
	if tx.Error != nil {
		return c.Status(500).SendString(tx.Error.Error())
	}

	// ! This code is rather dumb.
	var highestEntry AuditLog
	highestID := -1

	for _, entry := range query.AuditLog {
		if int(entry.ID) > highestID {
			highestID = int(entry.ID)
			highestEntry = entry
		}
	}

	return c.JSON(highestEntry)
}

// We break a pattern by using AuditLog instead of Person here.
func editAudit(c *fiber.Ctx) error {
	id := c.Params("id")
	if id == "" {
		return fiber.ErrBadRequest
	}
	var Audit AuditLog
	tx := db.First(&Audit, "id = ?", id)
	if tx.Error != nil {
		return c.Status(500).SendString(db.Error.Error())
	}
	err = c.BodyParser(&Audit)
	if err != nil {
		return c.Status(500).SendString(err.Error())
	}
	tx.Updates(&Audit)
	if tx.Error != nil {
		return c.Status(500).SendString(db.Error.Error())
	}

	return c.JSON(Audit)
}
func getAudit(c *fiber.Ctx) error {
	id := c.Params("id")
	if id == "" {
		return fiber.ErrBadRequest
	}

	var Audit AuditLog
	db.First(&Audit, "id = ?", id)
	if db.Error != nil {
		return c.Status(500).SendString(db.Error.Error())
	}
	return c.JSON(Audit)
}
func deleteAudit(c *fiber.Ctx) error {
	id := c.Params("id")
	if id == "" {
		return fiber.ErrBadRequest
	}
	var Audit AuditLog
	tx := db.First(&Audit, "id = ?", id)
	if tx.Error != nil {
		return c.Status(500).SendString(tx.Error.Error())
	}
	tx.Delete(&Audit)

	return c.JSON(Audit)
}

func getRelationships(c *fiber.Ctx) error {
	idParam := c.Params("id")
	if idParam == "" {
		return fiber.ErrBadRequest
	}

	var id uint
	id, err = stringtoid(idParam)
	if err != nil {
		return c.Status(500).SendString(err.Error())
	}

	return c.JSON(relationshipHandler(id))
}

func relationshipHandler(id uint) []Relationship {
	var dbrelationships []Relationship
	db.Where("person_id = ?", id).Or(
		db.Where("to_person_id = ?", id)).Find(&dbrelationships)

	var relationships []Relationship

OUTER:
	// Us rearranging relationships below could cause duplicates, so let's deduplicate proactively.
	for _, dbrelationship := range dbrelationships {
		for _, apirelationship := range relationships {
			if dbrelationship.ToPersonID == apirelationship.ToPersonID {
				continue OUTER
			}
		}

		// The API only returns ToPersonID, so if we are the target of a dbrelationship,
		// we need to reverse it
		if dbrelationship.ToPersonID == id {
			relationships = append(relationships, Relationship{
				ToPersonID:       dbrelationship.PersonID,
				RelationshipType: dbrelationship.RelationshipType,
			})
		} else {
			relationships = append(relationships, dbrelationship)
		}

	}

	return relationships
}

func getPersonIndex(c *fiber.Ctx) error {
	// * If we get an account system, this will have to be scoped to the account,
	// * which does make this a bit more complex. We can get away with this right now.
	var dbpeople []Person
	status := c.Query("status")

	tx := db.Preload(clause.Associations)
	if status != "" {
		tx.Where("status = ?", status)
	}
	tx.Find(&dbpeople)

	var people []Person
	for _, person := range dbpeople {
		person.Relationship = relationshipHandler(person.ID)
		people = append(people, person)
	}

	return c.JSON(people)
}
