export const BASEURL = window.location.origin
// We should probably allow the user to set this somehow.
export const LOCALE = "de-DE"

export const TIMEZONE = "Europe/Berlin"