import {BASEURL} from "./constants.mjs";
import {generatePerson, getDB} from "./db.mjs";
const queryString = window.location.search;
const urlParams = new URLSearchParams(queryString);
const button = document.getElementById("button")
const name = document.getElementById("name")
const nick = document.getElementById("nick")
const prefsNick = document.getElementById("nickPref")
const dob = document.getElementById("dob")
const pronouns = document.getElementById("pronouns")
const gender = document.getElementById("gender")
const employment = document.getElementById("employment")
const summary = document.getElementById("summary")
let dateOfBirth = new Date()
const db = await getDB()
const id = +urlParams.get('id');

const result = await db.person.get(id)
await prefillData(result)

async function prefillData(data) {
    dateOfBirth = new Date(data.dateOfBirth)
    name.value = data.name
    nick.value = data.nickname
    dob.value = dateOfBirth.toISOString().split("T")[0]
    pronouns.value = data.pronouns
    gender.value = data.gender
    employment.value = data.employment
    summary.value = data.summary
}

async function submitChanges() {
    const request = {
        "name": name.value,
        "nickname": nick.value,
        "prefersNickname": prefsNick.checked,
        "dateOfBirth": dateOfBirth.toISOString(),
        "pronouns": pronouns.value,
        "gender": gender.value,
        "employment": employment.value,
        "summary": summary.value,
    }
    if (dob.value !== dateOfBirth.toISOString().split("T")[0]) {
        request.dateOfBirth = new Date(dob.value).toISOString()
    }
    let requestJSON = JSON.stringify(request)
    const response = await fetch(`${BASEURL}/person/${id}`, {
        method: 'PATCH',
        body: requestJSON,
        headers: {
            'Content-type': 'application/json; charset=UTF-8',
        },
    })
    const responseObj = await response.json()
    const personObj = await generatePerson(responseObj, null)
    await db.person.update(id, personObj)
    window.location.href = `${BASEURL}/person.html?id=${id}`

}

button.addEventListener("click", submitChanges)