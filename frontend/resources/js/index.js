import {BASEURL} from "./constants.mjs";
import {getDB} from "./db.mjs";


const contactsObj = document.getElementById("contacts")

const db = await getDB()
const result = await db.person.toArray()
await createElement(result)

async function createElement(data) {
    const count = document.getElementById("count")
    count.textContent = data.length + " contacts"

    for (const person of data) {
        const container = document.createElement("section")


        const avatar = document.createElement("img")
        avatar.setAttribute("src", URL.createObjectURL(person.avatar))


        const name = document.createElement("a")
        if (person.prefersNickname) {
            name.textContent = person.nickname
        } else {
            name.textContent = person.name
        }
        name.setAttribute("href", `${BASEURL}/person.html?id=${person.ID}`)

        container.classList.add("person_card")
        avatar.classList.add("avatar")

        container.appendChild(avatar)
        container.appendChild(name)
        contactsObj.appendChild(container)
    }
}

