import {BASEURL, LOCALE, TIMEZONE} from "./constants.mjs";
import * as dbhelper from "./db.mjs";

const queryString = window.location.search;
const urlParams = new URLSearchParams(queryString);
const PersonId = +urlParams.get('id')
const button = document.getElementById("editbutton")
const decombutton = document.getElementById("decom")

let isDecom = true


const db = await dbhelper.getDB()
const personCache = await db.person.get(PersonId)
await createElement(personCache)


// Method is highly reusable, move into global context?
function createErrorBanner(error) {
    const text = document.createElement("p")
    text.textContent = error
    text.style.backgroundColor = "#D71000"
    document.getElementById("header").appendChild(text)
}

// This sucks.
function getAgeFromDate(parsedDob) {
    const difference = Date.now() - parsedDob.getTime()
    const ageDate = new Date(difference)
    return Math.abs(ageDate.getUTCFullYear() - 1970)
}

// This is a god method, this should get split up and refactored.
async function createElement(data) {
    const name = document.getElementById("title")

    if (data.prefersNickname === true) {
        name.textContent = data.nickname
    } else {
        name.textContent = data.name
    }

    let status = document.getElementById("status")
    switch (data.status) {
        case 0:
            status.textContent = "Active"
            status.classList.add("badge", "badge-good")
            decombutton.textContent = "Decommission"
            decombutton.classList.add("button-bad")
            isDecom = false
            break
        case 1:
            status.textContent = "Decommissioned"
            status.classList.add("badge-info")
            decombutton.textContent = "Recommission"
            decombutton.classList.add("btn")
            button.disabled = true
            break
        case 2:
            status.textContent = "Req. Takedown"
            status.classList.add("badge-error")
            decombutton.textContent = "Recommission"
            decombutton.classList.add("btn")
            break
    }


    let avatar = document.getElementById("avatar")

    data.avatarURL = URL.createObjectURL(data.avatar)
    avatar.setAttribute("src", data.avatarURL)

    const employment = document.getElementById("occupation")
    employment.textContent = `Employed as: ${data.employment}`

    const gendershit = document.getElementById("gender")
    gendershit.textContent = `${data.pronouns} (Gender: ${data.gender})`

    const dob = document.getElementById("dob")
    const parsedDob = new Date(data.dateOfBirth)
    const calculatedAge = getAgeFromDate(parsedDob);
    dob.textContent = `Age: ${calculatedAge} (${parsedDob.toLocaleDateString(LOCALE)})`

    const description = document.getElementById("description")
    description.textContent = data.summary


    const links = document.getElementById("links")
    for (const acc of data.accounts) {
        let accounts = document.createElement("a")
        if (acc.uri !== "") {
            accounts.setAttribute("href", acc.uri)
            let parsed = new URL(acc.uri)
            const favicon = document.createElement("img")
            favicon.setAttribute("src", `https://icons.duckduckgo.com/ip3/${parsed.host}.ico`)
            favicon.classList.add("link-icon")
            accounts.appendChild(favicon)
            links.appendChild(accounts)
        }
    }

    const newauditbutton = document.getElementById("newaudit")
    if (isDecom) {
        newauditbutton.disabled = true
    }
    newauditbutton.addEventListener("click", function () {
        window.location.href = `${BASEURL}/audit.html?id=${PersonId}&creating=true`
    })


    const fetchrelationships = await fetch(`${BASEURL}/person/${PersonId}/relationship`)
    const relationships = await fetchrelationships.json()
    const RelationshipObj = document.getElementById("relationships")
    for (const relationship of relationships) {
        const personName = await fetch(`${BASEURL}/person/${relationship.ToPersonID}/name`)
        if (personName.status !== 200) {
            continue
        }
        let text = document.createElement("p")
        switch (relationship.RelationshipType) {
            case 0:
                text.textContent = "Friend of "
                break
            case 1:
                text.textContent = "Spouse of "
                break
            case 2:
                text.textContent = "Partner of "
                break
            case 3:
                text.textContent = "Ex partner of "
                break
            case 4:
                text.textContent = "Colleague of "
                break
        }
        let person = document.createElement("a")
        person.textContent = await personName.text()
        person.setAttribute("href", `${BASEURL}/person.html?id=${relationship.ToPersonID}`)

        text.appendChild(person)
        RelationshipObj.appendChild(text)
    }


    const auditsObj = document.getElementById("scrollable")
    const audits = await db.audit.where("personid").equals(PersonId).toArray()

    for (const audit of audits) {
        const container = document.createElement("div")
        const auditText = document.createElement("p")
        const auditDate = document.createElement("p")
        const auditType = document.createElement("h5")
        auditText.textContent = audit.content

        switch (audit.type) {
            case 0:
                auditType.textContent = "System"
                auditType.classList.add("badge", "badge-info")
                break
            case 1:
                auditType.textContent = "Event"
                auditType.classList.add("badge")
                break
            case 2:
                auditType.textContent = "Ramble"
                auditType.classList.add("badge", "badge-good")
                break
        }


        auditDate.textContent = `Added ${new Date(audit.date).toLocaleString(LOCALE, {timeZone: TIMEZONE})}`

        const buttoncontainer = document.createElement("div")
        buttoncontainer.classList.add("buttoncontainer")

        const editButton = document.createElement("button")
        editButton.setAttribute("type", "button")
        editButton.textContent = "Edit"
        editButton.value = audit.ID
        editButton.addEventListener("click", async function () {
            window.location.href = `${BASEURL}/audit.html?id=${this.value}`
        })
        editButton.classList.add("btn")

        const deleteButton = document.createElement("button")
        deleteButton.setAttribute("type", "button")
        deleteButton.textContent = "Delete"
        deleteButton.value = audit.ID
        deleteButton.addEventListener("click", async function () {
            const request = await fetch(`${BASEURL}/audit/${this.value}`, {
                method: "DELETE"
            })
            if (!request.ok) {
                createErrorBanner("The server rejected the request, cache is probably stale")
                console.debug(`${request.status}: ${await request.text()}`)
                return
            }
            await db.audit.delete(parseInt(this.value))
            // noinspection JSCheckFunctionSignatures
            location.reload(true)
        })
        deleteButton.classList.add("btn", "button-bad")

        if (isDecom) {
            deleteButton.disabled = true
            editButton.disabled = true
        }

        buttoncontainer.append(editButton, deleteButton)
        container.append(auditType, auditDate, auditText, buttoncontainer)
        auditsObj.appendChild(container)
    }
}

async function editButton() {
    const url = new URL(`${BASEURL}/edit.html`);
    url.searchParams.set('id', PersonId);
    window.location.href = url.toString()
}

async function decomButton() {
    const url = new URL(`${BASEURL}/person/${PersonId}`);
    if (!isDecom) {
        url.pathname += "/decom"
        await fetch(url, {
            method: "POST"
        })
        personCache.status = 1
        await db.person.update(personCache.ID, personCache)
    } else {
        url.pathname += "/recom"
        await fetch(url, {
            method: "POST"
        })
        personCache.status = 0
        await db.person.update(personCache.ID, personCache)
    }
    // noinspection JSCheckFunctionSignatures
    location.reload(true)
}

button.addEventListener("click", editButton)
decombutton.addEventListener("click", decomButton)