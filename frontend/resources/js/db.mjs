import {BASEURL} from "./constants.mjs";
import Dexie from "https://unpkg.com/dexie@3.2.4/dist/dexie.mjs";

export async function getDB() {
    const db = new Dexie('cache');
    db.version(1).stores({
        person: `ID, status`,
        audit: `ID, personid`
    })

    if (await db.person.count() === 0) {
        const index = await fetch(`${BASEURL}/person/index`)
        const indexdata = await index.json()
        const indexdataLength = indexdata.length
        // We want Avatar to be a blob to not do a network fetch, so we'll have to replace it here
        for (let i = 0; i < indexdataLength; i++) {
            indexdata[i] = await generatePerson(indexdata[i], db)
        }
        await db.person.bulkAdd(indexdata)
    }
    return db
}

export async function refreshDB() {
    await getDB().then((async db => {
        await db.delete()
    }));
    location.reload()
}

export async function generatePerson(person, db) {
    if (db != null) {
        db.audit.bulkAdd(person.audit)
        delete person.audit
    }

    // * This is not a blob yet, so the type assumption is wrong from IDEA.
    // noinspection JSCheckFunctionSignatures
    const avatar = await fetch(person.avatar)
    person.avatar = await avatar.blob()
    return person
}
