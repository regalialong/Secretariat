import {BASEURL} from "./constants.mjs";
import * as dbhelper from "./db.mjs";
const queryString = window.location.search;
const urlParams = new URLSearchParams(queryString);
const creating = !!urlParams.get('creating')
const id = +urlParams.get('id')

const status = document.getElementById("status")
const content = document.getElementById("content")
const date = document.getElementById("date")
const datenow = document.getElementById("datenow")
const datenownotime = document.getElementById("datenownotime")
const submit = document.getElementById("submit")

const db = await dbhelper.getDB()

if (!creating) {
    const audit = await db.audit.get(+id)
    await populateAuditFields(audit)
}

async function populateAuditFields(audit) {
    switch (audit.type) {
        case 0:
            status.value = "System"
            break
        case 1:
            status.value = "Event"
            break
        case 2:
            status.value = "Ramble"
            break
    }

    content.textContent = audit.content
    const parsedDate = new Date(audit.date)
    parsedDate.setMilliseconds(0) // We don't need that much precision, thanks server.
    parsedDate.setSeconds(0)
    date.valueAsNumber = parsedDate.getTime()
}

async function submitButton() {
    let type = 0
    // We repeat this reversed, make a better pattern for this.
    switch (status.value) {
        case "System":
            type = 0
            break
        case "Event":
            type = 1
            break
        case "Ramble":
            type = 2
            break
    }

    const sendObj = {
        "type": type, "content": content.value, date: null
    }
    if (date.value !== "") {
        sendObj.date = new Date().toISOString()
    }


    const fetchoptions = {
        method: "PATCH", body: JSON.stringify(sendObj), headers: {
            'Content-type': 'application/json; charset=UTF-8',
        }
    }

    let fetchURL = `${BASEURL}/audit/${id}`
    if (creating) {
        fetchURL = `${BASEURL}/person/${id}/audit`
        fetchoptions.method = "POST"
    }
    const response = await fetch(fetchURL, fetchoptions)
    const responseObj = await response.json()
    if (creating) {
        db.audit.add(responseObj)
    } else {
        await db.audit.update(id, responseObj)
    }


    window.location.href = `${BASEURL}/person.html?id=${responseObj.personid}`
}

submit.addEventListener("click", submitButton)

datenow.addEventListener("click", await async function () {
    date.value = new Date().toLocaleString("sv-SE", {
        year: "numeric",
        month: "2-digit",
        day: "2-digit",
        hour: "2-digit",
        minute: "2-digit",
        second: "2-digit"
    }).replace(" ", "T")
})
datenownotime.addEventListener("click", await async function () {
    date.value = new Date().setHours(0, 0, 0, 0).toLocaleString("sv-SE", {
        year: "numeric",
        month: "2-digit",
        day: "2-digit",
        hour: "2-digit",
        minute: "2-digit",
        second: "2-digit"
    }).replace(" ", "T")
})