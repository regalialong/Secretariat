package main

import (
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/compress"
	"github.com/gofiber/fiber/v2/middleware/etag"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
	"log"
	"os"
	"time"
)

var (
	db  *gorm.DB
	err error
)

func main() {
	app := fiber.New(fiber.Config{
		AppName: "Secretariat",
	})
	app.Use(etag.New())
	app.Use(compress.New())

	app.Static("/avatars", "./avatars")
	app.Static("/", "./frontend")

	newLogger := logger.New(
		log.New(os.Stdout, "\r\n", log.LstdFlags), // io writer
		logger.Config{
			SlowThreshold:             time.Second,
			LogLevel:                  logger.Info,
			IgnoreRecordNotFoundError: false,
			ParameterizedQueries:      true,
			Colorful:                  true,
		},
	)

	db, err = gorm.Open(sqlite.Open("gorm.db"), &gorm.Config{Logger: newLogger})
	err = db.AutoMigrate(&Person{}, &OnlineAccount{}, &AuditLog{}, &Relationship{})
	if err != nil {
		panic(err)
	}

	person := app.Group("/person")

	person.Get("/index", getPersonIndex)
	person.Post("/", createPerson)
	person.Patch("/:id", updatePerson)
	person.Get("/:id", queryPerson)
	person.Delete("/:id", deletePerson)
	person.Get("/:id/name", getPersonName)

	person.Post("/:id/decom", managePersonDecomStatus)
	person.Post("/:id/recom", managePersonDecomStatus)

	person.Get("/:id/audit", queryAudit)
	person.Post("/:id/audit", addAudit)

	person.Get("/:id/relationship", getRelationships)

	app.Get("/audit/:id/", getAudit)
	app.Patch("/audit/:id", editAudit)
	app.Delete("/audit/:id", deleteAudit)

	err = app.Listen(":3000")
	if err != nil {
		return
	}
}
